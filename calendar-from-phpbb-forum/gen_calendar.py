#!/usr/bin/env python3

# INSTALL: pip install beautifulsoup4 jinja2 python-dateutil requests

import os
from collections import defaultdict
from datetime import date as Date

from bs4 import BeautifulSoup
from dateutil.parser import parserinfo, parse as parse_date
from jinja2 import Environment, FileSystemLoader
import pytest
import requests


FORUM_BASE_URL = 'https://laubergedesreveurs.forumactif.com'
FORUM_SESSIONS_SECTION_URLS = (
    'https://laubergedesreveurs.forumactif.com/f20-parties-et-activites-a-venir-ou-en-cours',
    'https://laubergedesreveurs.forumactif.com/f16-archives-evenements',
)
LOGO_URL = 'logo-big.png'
THIS_YEAR = Date.today().year
PARENT_DIR = os.path.dirname(os.path.realpath(__file__))
OUTPUT_HTML_FILENAME = f'{PARENT_DIR}/../index-with-events.html'


def main():
    os.chdir(PARENT_DIR)
    events = get_events()
    events_per_date = defaultdict(list)
    for event in events:
        events_per_date[event["date"]].append(event)
    years = sorted(set(date[:4] for date in events_per_date))
    for year in years:
        count = sum(1 for event in events if event["date"][:4] == year)
        print(f"* {year}: {count} events found")
    for date, events_for_date in events_per_date.items():
        if len(events_for_date) > 1:
            print(f"[WARNING] {date} - Seul le 1er événement apparaîtra dans le calendrier : " + ", ".join(
                event["label"] for event in events_for_date
            ))
    env = Environment(loader=FileSystemLoader(PARENT_DIR + '/..'))
    template = env.get_template('index.html')
    with open(OUTPUT_HTML_FILENAME, 'w', encoding='utf8') as html_file:
        html_file.write(template.render(events=events, home_url=FORUM_BASE_URL, logo_url=LOGO_URL))
    print('Generated:', OUTPUT_HTML_FILENAME)


def get_events():
    events = []
    log_warns = True
    for url in FORUM_SESSIONS_SECTION_URLS:
        events.extend(extract_events(url, log_warns=log_warns))
        log_warns = False
    return events

def extract_events(url, year=None, log_warns=False):
    resp = requests.get(url, timeout=10)
    resp.raise_for_status()
    soup = BeautifulSoup(resp.text, 'html.parser')
    events = []
    for title in soup.findAll(class_='topic-title'):
        anchor = list(title.children)[0]
        label = anchor.get_text()
        try:
            game, date, _ = parse_label(label)
            if year:
                date = date.replace(year=year)
            print('✔️', label, '->', date.strftime('%Y-%m-%d'))
            events.append({
                'url': FORUM_BASE_URL + anchor['href'],
                'label': game,
                'date': date.strftime('%Y-%m-%d'),
            })
        except (IndexError, ValueError) as error:
            if log_warns:
                print("[WARNING] Ce nom de thread n'a pas le bon format: " + label, error)
    for sousforum in soup.findAll('a', class_='forumlink'):
        subforum_year = year
        if ' 20' in sousforum.get_text():
            subforum_year = int(sousforum.get_text().split(' ')[-1])
        events.extend(extract_events(FORUM_BASE_URL + sousforum['href'], year=subforum_year))
    return events


def parse_label(label):
    # Format attendu : [Nom du jeu] - Date - autres infos
    game, desc = label[1:].split(']', 1)
    desc_frags = desc.split('-', 2)
    if len(desc_frags) == 2:
        date = desc_frags[1]
        info = ""
    else:
        _, date, info = desc_frags
    date = parse_date(date, fuzzy=True, parserinfo=FrenchParserInfo(dayfirst=True))
    return game.strip(), date, info.strip()


class FrenchParserInfo(parserinfo):
    HMS = [('h', 'heure', 'heures'), ('m', 'minute', 'minutes'), ('s', 'seconde', 'secondes')]
    JUMP = [' ', '.', ',', ';', '-', '/', "'", 'le', 'à', 'de', 'et']
    MONTHS = [('Jan', 'Janvier'), ('Fev', 'Fevrier', 'Février'), ('Mar', 'Mars'),
              ('Avr', 'Avril'), ('Mai', 'Mai'), ('Jui', 'Juin'),
              ('Jui', 'Juillet'), ('Aou', 'Aout', 'Août'), ('Sep', 'Septembre'),
              ('Oct', 'Octobre'), ('Nov', 'Novembre'), ('Dec', 'Decembre', 'Décembre')]
    WEEKDAYS = [("Lun", "Lundi"),
                ("Mar", "Mardi"),
                ("Mer", "Mercredi"),
                ("Jeu", "Jeudi"),
                ("Ven", "Vendredi"),
                ("Sam", "Samedi"),
                ("Dim", "Dimanche")]


def test_extract_date():
    game, date, info = parse_label('[COPS] - Samedi 11 décembre - à partir de 21h30 - scénario pilote')
    assert game == 'COPS'
    assert (date.day, date.month, date.year) == (11, 12, THIS_YEAR)
    assert info == 'à partir de 21h30 - scénario pilote'

    game, date, info = parse_label('[Jeu] - Samedi 6 août 2022')  # 2e tiret manquant
    assert game == 'Jeu'
    assert (date.day, date.month, date.year) == (6, 8, 2022)
    assert info == ''

    with pytest.raises(ValueError):
        assert parse_label('Les Hauts-Royaumes - Week-end du 18/19 sept, date à définir')


if __name__ == '__main__':
    main()

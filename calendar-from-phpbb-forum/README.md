# Usage

Edit the constants at the top of the `gen_calendar.py` script and run it.

## Installation

    pip install -r requirements.txt

## Unit tests

    pytest gen_calendar.py

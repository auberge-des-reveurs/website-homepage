[![pipeline status](https://framagit.org/auberge-des-reveurs/website-homepage/badges/main/pipeline.svg)](https://framagit.org/auberge-des-reveurs/website-homepage/commits/main)

Sources de la page d'accueil de https://laubergedesreveurs.forumactif.com

Un peu de contexte / explications :
https://chezsoi.org/lucas/blog/script-de-mise-a-jour-de-page-daccueil-forumactif.html

<!-- Ideas for extra stuff:
* adapt / introduce Python script to permform conversion of all src= href= links from relative URLs to absolute ones,
  to allow for local development using relative URLs (not currently possible)
* ajouter lien vers la "version mobile" du forum : s'agit-il de la PWA ? https://laubergedesreveurs.forumactif.com/admin/?mode=webapp&part=modules&sub=topicit&tid=749055b2a484f836d830d033b893fc93&_tc=1651917374
* les liens de nav redirigent vers frama.io
* "since 2019" parfois mal placé sur smartphone
* outlines en pointillées moches
* lorsqu'on est en fin d'année 2021 (novembre, décembre), les nouvelles parties proposées pour l'année suivantes sont insérées en 2021 et non 2022
* Add HTML linter
-->

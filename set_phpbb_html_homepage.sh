#!/bin/bash
# Enabling Bash strict mode:
set -o errexit -o nounset -o pipefail
urlencode () {  # Bash function to perform URLencoding on stdin
    python3 -c 'import sys; from urllib.parse import quote_plus; sys.stdout.writelines([quote_plus(line) for line in sys.stdin])'
}
# Authenticating & storing the sid cookie in a file:
curl -Ls --fail -c cookies.txt https://laubergedesreveurs.forumactif.com/login --data-raw "username=${USERNAME:?}&password=${PASSWORD:?}&autologin=on&redirect=&query=&login=Connexion" -o out.html
# Ensuring auth was successful:
grep -qF $USERNAME out.html || { echo "Login as $USERNAME failed"; exit 1; }
# Extracting the tid:
tid=$(grep -F tid= out.html | sed 's/.*tid=\([^&]\+\)&.*/\1/' | head -n 1)
# Uploading a new version of index.html:
curl -Ls --fail -b cookies.txt "https://laubergedesreveurs.forumactif.com/admin/?part=modules&sub=html&extended_admin=1&tid=$tid" --data-raw "title=Homepage&forumact_template=0&set_homepage=1&html=$(urlencode < index.html)&mode=go_edit&editor=wysiwyg&page=1&submit=Valider" -o out.html
# Ensuring upload was successful:
grep -qF "Votre page HTML a bien été modifiée" out.html || { echo "Homepage update failed"; exit 1; }
# Final cleanup:
rm cookies.txt out.html

#!/usr/bin/env python3

# The goal of those bundles is:
# * package resources in a single file to limit network I/O required to fetch them
# * reflect any change in their content by a filename change
#   to benefit from maximum resources caching through HTTP headers
# USAGE: rm assets/*/bundle* && ./gen_statics_bundles.py && git add assets/*/bundle*

import re
from hashlib import sha1


CSS_SRCS = (
    'assets/css/main.css',  # WARN: @imports must come first
    'calendar-from-phpbb-forum/calendar-bundle.css',
)
JS_SRCS = (
    'calendar-from-phpbb-forum/calendar-bundle.js',
    'assets/js/jquery.min.js',
    'assets/js/jquery.scrolly.min.js',
    'assets/js/jquery.scrollex.min.js',
    'assets/js/browser.min.js',
    'assets/js/breakpoints.min.js',
    'assets/js/roll-a-die.js',
    'assets/js/util.js',
    'assets/js/main.js',
)

def gen_css_bundle():
    short_hash = sha1(b''.join(cat(css_file) for css_file in CSS_SRCS)).hexdigest()[:7]
    css_bundle_filepath = f'assets/css/bundle-{short_hash}.css'
    with open(css_bundle_filepath, 'wb') as bundle:
        for css_file in CSS_SRCS:
            bundle.write(cat(css_file))
    sed('index.html', 'assets/css/bundle-[a-z0-9]+.css', f'assets/css/bundle-{short_hash}.css')

def gen_js_bundle():
    short_hash = sha1(b''.join(cat(js_file) for js_file in JS_SRCS)).hexdigest()[:7]
    js_bundle_filepath = f'assets/js/bundle-{short_hash}.js'
    with open(js_bundle_filepath, 'wb') as bundle:
        for js_file in JS_SRCS:
            bundle.write(cat(js_file))
    sed('index.html', 'assets/js/bundle-[a-z0-9]+.js', f'assets/js/bundle-{short_hash}.js')

def cat(filepath):
    with open(filepath, 'rb') as file:
        return file.read()

def sed(filepath, pattern, value):
    with open(filepath, 'r+', encoding='utf8') as file:
        data = file.read()
        file.seek(0)
        file.write(re.sub(pattern, value, data))
        file.truncate()

if __name__ == '__main__':
    print('Generating CSS bundle')
    gen_css_bundle()
    print('Generating JS bundle')
    gen_js_bundle()
    print('END: index.html updated')
